//
//  PlayersCell.swift
//  BasketIOS
//
//  Created by Dejan Markovic on 1/15/19.
//  Copyright © 2019 Stefana Mladenovic. All rights reserved.
//

import UIKit

class PlayersCell: UITableViewCell {
    
    @IBOutlet weak var playerNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
