//
//  RegistrationViewController.swift
//  BasketIOS
//
//  Created by Dejan Markovic on 1/14/19.
//  Copyright © 2019 Stefana Mladenovic. All rights reserved.
//
import UIKit
import Foundation
import Firebase

class RegistrationViewColtroller: UIViewController{
    
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    
    @IBAction func registerUser(_ sender: Any) {
        
        Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { (user, error) in
            if(error != nil){
                print(error!)
            } else {
               let request =  Auth.auth().currentUser?.createProfileChangeRequest()
                request?.displayName = self.usernameTextField.text!
                request?.commitChanges(completion: { (error) in
                    
                    print("Registration Successful!")
                    self.performSegue(withIdentifier: "registrationComplete", sender: self)
                })
            }
        }
    }
    
    override func viewDidLoad(){
        
    }
}
