//
//  ListOfPlayersController.swift
//  BasketIOS
//
//  Created by Dejan Markovic on 1/15/19.
//  Copyright © 2019 Stefana Mladenovic. All rights reserved.
//
import Firebase

class ListOfPlayersController : UIViewController {
    
    var appointment = AppointmentDTO(){
        didSet{
            loadPlayers()
        }
    }
    var playersArray : [String] = [String]()

    @IBOutlet weak var prijava: UIButton!
    
    @IBOutlet weak var table: UITableView!
    
    override func viewDidLoad(){
        table.delegate = self
        table.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let user = Auth.auth().currentUser
        
        let displayname = (Auth.auth().currentUser?.displayName)!
        
        
        if(self.playersArray.contains(displayname)) {
            
            self.prijava.setTitle("Odjava", for: .normal)
        
        } else {
            self.prijava.setTitle("Prijava", for: .normal)
        }
    }
    
    func loadPlayers(){
        Database.database().reference().child(appointment.id).child("players").observe(.value) { (snapshot) in
            self.playersArray.removeAll()
            for rest in snapshot.children.allObjects as! [DataSnapshot] {
                self.playersArray.append(rest.key)
                self.table.reloadData()
            }
        }
        
    }
    
    
    @IBAction func prijavaTapped(_ sender: Any) {
        //na bazu firebase dodajem jos jednog igraca (curent user) na playere za taj appointment
    
        let displayname = (Auth.auth().currentUser?.displayName)!
        
        if playersArray.contains(displayname) {
            prijava.setTitle("Prijava", for: .normal)
            
            playersArray.remove(at: playersArray.firstIndex(of: displayname)!)

        } else {
            
            playersArray.append(displayname)

            prijava.setTitle("Odjava", for: .normal)
        }
        var data : Dictionary<String, String> = [:]
        
        for player in playersArray{
            data[player] = player
        }
        
        Database.database().reference().child(appointment.id)
            .child("players").setValue(data)
        
        table.reloadData()
    }
}

extension ListOfPlayersController : UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playersArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "playerCell", for: indexPath) as! PlayersCell
        cell.playerNameLabel.text = "\(indexPath.row+1).\( playersArray[indexPath.row])"
        
        if(indexPath.row % 2 == 0){
            cell.backgroundColor = UIColor.lightGray
        }else{
            cell.backgroundColor = UIColor.white
        }
        
        return cell
    }
}
