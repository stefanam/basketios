//
//  BasketAppointmentsViewController.swift
//  BasketIOS
//
//  Created by Dejan Markovic on 1/13/19.
//  Copyright © 2019 Stefana Mladenovic. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import Floaty
import DateTimePicker

class BasketAppointmentsViewController: UIViewController {
    
    
    @IBOutlet weak var table: UITableView!
    var appointmentsArray : [AppointmentDTO] = [AppointmentDTO]()
    var selectedPosition = 0;
  //  let button = MDCButton()
    
    override func viewDidLoad(){
        table.delegate = self
        table.dataSource = self
        
        let floaty = Floaty()
    
        
        floaty.addItem(title: "Add new appointment") { item in
            self.showAlert()
        }
        
        self.view.addSubview(floaty)

        configureTableView()
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Database.database().reference().observe(.value) { (snapshot) in
            self.appointmentsArray.removeAll()
            for rest in snapshot.children.allObjects as! [DataSnapshot] {
                
            let appointment = AppointmentDTO(snapshot: rest)!
            appointment.id = rest.key
            self.appointmentsArray.append(appointment)
            self.table.reloadData()
        }
        }
        
    }
    
    //date formater
    func formatGivenDate(givenDate: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        let formatedDate : String = formatter.string(from: givenDate)
        self.title = formatedDate
        print("Selected date: \(formatedDate)")
        return formatedDate
    }
    
    @IBAction func logOutPressed(_ sender: Any) {
        do{
            try Auth.auth().signOut()
            navigationController?.popToRootViewController(animated: true)
        } catch {
              print("Error. There was problem with signing out")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showListOfPlayers" {
            let vc = segue.destination as! ListOfPlayersController
            
            vc.appointment = appointmentsArray[self.selectedPosition]
            print("I selected category: \(vc.appointment)")
            
        }
    }
}

extension BasketAppointmentsViewController : UITableViewDelegate, UITableViewDataSource {
    
    
    func cellTapped(){
        //sacuvamo u fieldu poziciju da bi pristupili u prerpare for segue
        
        self.performSegue(withIdentifier: "showListOfPlayers", sender: self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appointmentsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "appointmentCell", for: indexPath) as! AppointmentTableViewCell
   
        let appointment = appointmentsArray[indexPath.row]
       
        cell.titleLabel.text = "\(self.formatGivenDate(givenDate: Date(timeIntervalSince1970: (Double(appointment.time) / 1000.0))))"
        cell.descriptionLabel.text = appointment.description
        cell.playerNumberLabel.text =  "Broj prijavljenih: \(appointment.players.count)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.selectedPosition = indexPath.row
        cellTapped()
        print((appointmentsArray[indexPath.row]))
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            
            let alert = UIAlertController(title: "Brisanje termina", message: "Da li ste sigurni da želite da izbrišete ovaj termin?", preferredStyle: .alert)
            
            
            //brisanje termina
            let delete = UIAlertAction(title: "Obriši", style: .default) { (action) in
                
        Database.database().reference().child(self.appointmentsArray[indexPath.row].id).removeValue()
                
//            self.appointmentsArray.remove(at: indexPath.row)
//                print(indexPath.row)

                print("Removed value: \(self.appointmentsArray[indexPath.row].id)" )
                

            }
            
            //ipak ne zelim da dodam termin
            let cancel = UIAlertAction(title: "Odustani", style: .default) { (action) in
            }
            
            alert.addAction(delete)
            alert.addAction(cancel)
            present(alert, animated: true, completion: nil)
            
        }
    }
    
    
    func configureTableView(){
        table.allowsSelection = true
        table.isUserInteractionEnabled = true
        table.register(UINib(nibName: "AppointmentTableViewCell", bundle: nil), forCellReuseIdentifier: "appointmentCell")
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = 120
    }
}

extension BasketAppointmentsViewController {
    
    //show alert dijalog za dodavanje novig termina
    func showAlert(){
        
        var newAppointment : AppointmentDTO = AppointmentDTO()
        var time : CLong = 0
        var description : String = ""
        //Naiv Alert dijaloga koji stoji na vrhu
        let alert = UIAlertController(title: "Novi termin", message: "", preferredStyle: .alert)
        
        //prvi text field gde se prikazuje datum i vreme
        alert.addTextField { (alertTextField) in
            alertTextField.placeholder = "Izaberi datum i vreme termina"
            alertTextField.isUserInteractionEnabled = false
            
            let min = Date().addingTimeInterval(-60 * 60 * 24 * 4)
            let max = Date().addingTimeInterval(480 * 60 * 24 * 4)
            let picker = DateTimePicker.create(minimumDate: min, maximumDate: max)
            picker.frame = CGRect(x: 0, y: 100, width: picker.frame.size.width, height: picker.frame.size.height)
            
            //date picker handler
            picker.completionHandler = { date in
                
                alertTextField.text = "\(self.formatGivenDate(givenDate: date))"
                
                time = CLong(picker.selectedDate.timeIntervalSince1970)
                newAppointment.time = time * 1000
                print(time)
            }
            
            self.view.addSubview(picker)
            picker.show()
            
        }
        
        //drugi text field gde unosim opis dodatog termina
        var textField = UITextField()
        
        alert.addTextField { (alertTextField) in
            alertTextField.placeholder = "Dodaj opis"
            textField = alertTextField
        }
        
        //dodavanje termina
        let add = UIAlertAction(title: "Dodaj novi termin", style: .default) { (action) in
            
            print("Description added \(textField.text ?? "nema opisa")")
            //dodati novi appointment
            description = textField.text!
            newAppointment.description = description
            
            let id = NSUUID().uuidString
            newAppointment.id = id
            Database.database().reference().child(id).setValue(newAppointment.toDictionary())
        }
        
        //ipak ne zelim da dodam termin
        let cancel = UIAlertAction(title: "Cancel", style: .default) { (action) in
        }
        
        alert.addAction(add)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
        
    }
}
