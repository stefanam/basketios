//
//  AppointmentDTO.swift
//  BasketIOS
//
//  Created by Dejan Markovic on 1/15/19.
//  Copyright © 2019 Stefana Mladenovic. All rights reserved.
//

import Firebase

class AppointmentDTO{
    
    var time : CLong = 0
    var description : String = ""
    var players : [String : String]
        = [:]
    var id : String = ""
    
    init?(snapshot: DataSnapshot) {
        guard let dic = snapshot.value as? [String:Any],
            let time = dic["startTime"] as? CLong,
            let description = dic["description"] as? String else {
                return nil
                //nazivi unutar stringova su nazivi fildova na firebase-u
                //nazivi fildova u dto klasi mogu biti sta god mi zelimo
                //sa ovom kastovima (as) smo povezali firebase fildove sa nasim fildovima
                //guard sluzi da ako ne dobijemo vrednosti vratimo null vrednosti za ceo objekat
        }
        self.time = time
        self.description = description
        self.players = dic["players"] as? Dictionary<String, String> ?? [:]
    }
    
    func toDictionary() -> Dictionary<String, Any>{
        
        var dictionary : Dictionary<String, Any> = [:]
        dictionary["startTime"] = time
        dictionary["description"] = description
        dictionary["id"] = id
        dictionary["players"] = players
        
        return dictionary as! Dictionary<String, Any>
    }
    
    init(){
    }
}
